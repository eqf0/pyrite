## TSPLIB file format. Described
## [here](http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/tsp95.pdf)

import std/[parseutils, options, sugar, streams], ../common
from std/strutils import parseEnum, Whitespace, strip

type
  TokKind {.pure.} = enum
    Word
    Val
    TokEof
  Token = object
    kind: TokKind
    str: string
  Tokenizer = object
    stream*: Stream
    currToken: Token

proc readBetween(s: Stream, pre: set[char], pos: set[char]): string =
  while (not s.atEnd):
    if peekChar(s) notin pre:
      break
    discard (readChar s)
  while (not s.atEnd):
    if peekChar(s) in pos:
      break
    result.add(readChar s)

proc nextToken(t: var Tokenizer,
               pre = WhiteSpace + {':'},
               pos = Whitespace + {':'}) =
  let val = strip(t.stream.readBetween(pre, pos))
  var n: float
  if val == "":
    t.currToken = Token(kind: TokEof, str: val)
  elif parseFloat(strip val, n) == 0:
    t.currToken = Token(kind: Word, str: val)
  else:
    t.currToken = Token(kind: Val, str: val)

proc initTokenizer(path: string): Tokenizer =
  result = Tokenizer(stream: newFileStream(path, fmRead))
  nextToken result

func word(t: Tokenizer): Option[string] =
  case t.currToken.kind
  of Word: some t.currToken.str
  else: none string

func val(t: Tokenizer): Option[float] =
  case t.currToken.kind
  of Val:
    var n: float
    discard parseFloat(t.currToken.str, n) # shoud have been verified before?
    some n
  else: none float

func rawStr(t: Tokenizer): string =
  t.currToken.str

func atEnd(t: Tokenizer): bool = t.currToken.kind == TokEof

proc readSeq(t: var Tokenizer): Option[seq[float]] =
  var r = newSeq[float]()
  while t.val.isSome:
    let n = get(t.val)
    if n == -1:
      nextToken t
      break
    else:
      r.add n
      nextToken t
  result = some r

type
  SpecField* {.pure.} = enum
    ## Fields allowed in the specification section of the file.
    Name = "NAME"
    Kind = "TYPE"
    Comment = "COMMENT"
    Dimention = "DIMENSION"
    Capacity = "CAPACITY"
    EdgeWeightType = "EDGE_WEIGHT_TYPE"
    EdgeWeightFormat = "EDGE_WEIGHT_FORMAT"
    EdgeDataFormat = "EDGE_DATA_FORMAT"
    NodeCoordType = "NODE_COORD_TYPE"
    DisplayDataType = "DISPLAY_DATA_TYPE"

  ProblemType* {.pure.} = enum
    ## The format allows theses types of optimization problems.
    Tsp = "TSP" ## Travelling Salesman Problem
    Atsp = "ATSP" ## Asymetric Travelling Salesman Problem
    Sop = "SOP" ## Sequential Ordering Problem
    Hcp = "HCP" ## Hamiltonean Cycle Problem
    Cvrp = "CVRP" ## Capacity Vehicule Routing Problem
    Tour = "TOUR" ## Not a problem. Just a collection of tours

  WeightType* {.pure.} = enum
    ## Type of distance function to be used with a `"FUNCTION"` edge weight type
    Explicit = "EXPLICIT" ## No function.
    Euc2d = "EUC_2D" ## Euclidean distance in 2d
    Euc3d = "EUC_3D" ## Euclidean distance in 3d
    Max2d = "MAX_2D" ## Maximum distance in 2d
    Max3d = "MAX_3D" ## Maximum distance in 3d
    Man2d = "MAN_2D" ## Manhattan distance in 2d
    Man3d = "MAN_3D" ## Manhattan distance in 3d
    Ceil2d = "CEIL_2D" ## Euclidean distance in 2d round up
    Geo = "GEO" ## Geographical distance
    Att = "ATT" ## Special distance function for `att48` and `att532`
    Xray1 = "XRAY1" ## Crystallography function
    Xray2 = "XRAY2" ## Crystallography function
    Special = "SPECIAL" ## Other

  WeightFormat* {.pure.} = enum
    ## Weights (if given explicitly) between nodes can be given in several ways.
    FullMatrix = "FULL_MATRIX" ## Expliciti weight matrix
    Function = "FUNCTION" ## Weights are calculated using a function
    UpperRow = "UPPER_ROW" ## Row-wise upper triangular matrix
    LowerRow = "LOWER_ROW" ## Row-wise lower triangular matrix
    UpperDiagRow = "UPPER_DIAG_ROW" ## Upper triangular with diagonal entries
    LowerDiagRow = "LOWER_DIAG_ROW" ## Lower triangular with diagonal entries
    UpperCol = "UPPER_COL" ## Column-wise upper triangular matrix
    LowerCol = "LOWER_COL" ## Column-wise lower triangular matrix
    UpperDiagCol = "UPPER_DIAG_COL" ## Upper triangular with diagonal entries
    LowerDiagCol = "LOWER_DIAG_COL" ## Lower triangullar with diagonal entries

  DataFormat* {.pure.} = enum
    ## If the graph is not complete, gives the format of the adjacencies
    EdgeList = "EDGE_LIST"
    AdjList = "ADJ_LIST"

  CoordType* {.pure.} = enum
    ## If the nodes have a position, give the format of the coordinates
    NoCoord = "NO_COORDS"
    Two = "TWOD_COORDS"
    Three = "THREED_COORDS"

  DisplayType* {.pure.} = enum
    ## How to obtain a graphical representation of the problem
    NoDisplay = "NO_DISPLAY" ## Not display possible
    Coord = "COORD_DISPLAY" ## Computed from coords
    Two = "TWOD_DISPLAY" ## 2d coords will be provided

  Spec* = object
    ## All specifications a TSPLIB file can have
    name*, comment*: string
    kind*: ProblemType
    dimention*: int
    capacity*: int ## For CVRP
    edgeKind*: WeightType
    edgeFormat*: WeightFormat ## For explicit weights
    edgeData*: DataFormat ## For non-complete graphs
    nodeCoord*: CoordType
    displayData*: DisplayType

func isSpecField(s: string): bool =
  # Replace with macro?
  case s
  of $Name, $Kind, $Comment, $Dimention, $Capacity, $EdgeWeightType: true
  of $EdgeWeightFormat, $EdgeDataFormat, $NodeCoordType, $DisplayDataType: true
  else: false

proc trySet(spec: var Spec, field, val: string): bool =
  ## The parsing is different for each field.
  ## TODO: replace `parseEnum` with something safe, custom macro?
  result = true
  case field
  of $Name: spec.name = val
  of $Kind: spec.kind = parseEnum[ProblemType] val
  of $Comment: spec.comment = val
  of $Dimention: result = parseInt(val, spec.dimention) > 0
  of $Capacity: result = parseInt(val, spec.capacity) > 0
  of $EdgeWeightType: spec.edgeKind = parseEnum[WeightType] val
  of $EdgeWeightFormat:
    spec.edgeFormat = parseEnum[WeightFormat] val
  of $EdgeDataFormat:
    spec.edgeData = parseEnum[DataFormat] val
  of $NodeCoordType: spec.nodeCoord = parseEnum[CoordType] val
  of $DisplayDataType: spec.displayData = parseEnum[DisplayType] val
  else: result = false

proc parseSpec(t: var Tokenizer): Option[Spec] =
  var spec = Spec()
  while t.rawStr.isSpecField:
    let key = t.word # first field must always be a keyword
    t.nextToken(pos = {'\n'}) # second field is the rest of the line
    if key.isNone:
      return none Spec

    let val = t.rawStr # second field may be whatever, not our job to verify
    nextToken t

    if not spec.trySet(get key, val):
      return none Spec

  result = some spec

type
  DataField {.pure.} = enum
    ## Data sections according to the previous specification
    NodeCoord = "NODE_COORD_SECTION" ## Specified by `CoordType`
    Depot = "DEPOT_SECTION" ## For CVRP
    Demand = "DEMAND_SECTION" ## For CVRP
    EdgeData = "EDGE_DATA_SECTION" ## Specified by `EdgeDataFormat`
    FixedEdges = "FIXED_EDGES_SECTION" ## Edges to be present in all solutions
    DisplayData = "DISPLAY_DATA_SECTION" ## Specifiede by `DisplayDataType`
    Tour = "TOUR_SECTION" ## Tours
    EdgeWeight = "EDGE_WEIGHT_SECTION" ## Specified by `EdgeWeightFormat`
    Eof = "EOF"

  Data* = object
    ## Stores the data inside a section
    nodeCoord*, edgeWeight*, edgeData*, depot*, tour*: seq[float]
    demand*, fixedEdges*, displayData*: seq[float]

proc trySet(d: var Data, key: string, vals: seq[float]): bool =
  result = true
  case key
  of $NodeCoord: d.nodeCoord = vals
  of $EdgeWeight: d.edgeWeight = vals
  of $EdgeData: d.edgeData = vals
  of $Depot: d.depot = vals
  of $DataField.Tour: d.tour = vals
  of $Demand: d.demand = vals
  of $FixedEdges: d.fixedEdges = vals
  of $DisplayData: d.displayData = vals
  else: result = false

proc parseData(t: var Tokenizer): Option[Data] =
  ## Parses the contents of a data section
  var data = Data()
  while not t.atEnd:
    let key = t.word
    nextToken t
    let vals = readSeq t
    nextToken t
    if key.isNone or vals.isNone:
      return none Data
    if not data.trySet(get key, get vals):
      return none Data
  result = some data

type
  GraphProblem* = ref object
    ## Result of parsing. Contains most information in the file, but probably
    ## the only relevant part is the weights matrix.
    name, comment: string
    weights: Matrix[float]
    display: Option[seq[(float, float)]]
    fixedEdges: seq[(int, int)]
    case kind: ProblemType
    of Cvrp:
      capacity: int
      depots: seq[int]
      demands: seq[(int, int)]
    of Sop:
      order: seq[(int, int)]
    else: discard

  TourInstance* = ref object
    ## Tour is weird. Contains more info than is should.
    spec: Spec
    tour: seq[int]

func newGraphProblem(s: Spec, d: Data): Option[GraphProblem] =
  # TODO: process data using spec to get all graph properties
  some GraphProblem()

func newTourInstance(s: Spec, d: Data): Option[TourInstance] =
  # TODO: validate data and extract tour fields
  some TourInstance()

proc parseFile(path: string): Option[(Spec, Data)] =
  var t = initTokenizer path
  let spec = parseSpec t
  let data = parseData t
  if spec.isNone or data.isNone:
    return none (Spec, Data)
  result = some (get spec, get data)

proc parseTour*(path: string): Option[TourInstance] =
  ## Reads file and tries to interpret it as a tour file.
  parseFile(path).flatMap:
    (sd: (Spec, Data)) => newTourInstance(sd[0], sd[1])

proc parseProblem*(path: string): Option[GraphProblem]
    {.raises: [IOError, OSError, ValueError, Exception].}=
  ## Reads file and tries to interpret it as some kind of problem.
  parseFile(path).flatMap:
    (sd: (Spec, Data)) => newGraphProblem(sd[0], sd[1])
