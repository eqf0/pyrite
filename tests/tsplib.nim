import pyrite/formats/tsplib {.all.}
import std/[streams, unittest, options]
from std/strutils import unindent

proc stringTokenizer(s: string): Tokenizer =
  result = Tokenizer(stream: newStringStream s)
  nextToken result

suite "tokens":
  test "parses all input":
    var t = newStringStream "k"
    check(t.readBetween({}, {}) == "k")

  test "parses string with prefix whitespace":
    var t = newStringStream " k"
    check(t.readBetween({' '}, {' '}) == "k")

  test "parses string surrounded by whitespace":
    var t = newStringStream " k j  "
    check t.readBetween({' '}, {' '}) == "k"
    check t.readBetween({' '}, {' '}) == "j"
    check t.readBetween({' '}, {' '}) == ""

  test "init file tokenizer":
    discard initTokenizer "data/ulysses16.tsp"

  test "ignores separators":
    var t = stringTokenizer "  \n \t : \n\n : \t"
    check t.atEnd

  test "tokenize words":
    var t = stringTokenizer "a a a a"
    while not t.atEnd:
      check t.word.isSome
      check get(t.word) == "a"
      nextToken t

  test "tokenize values":
    var t = stringTokenizer "1 1.0 1 1.0"
    while not t.atEnd:
      check t.val.isSome
      check get(t.val) == 1
      nextToken t

  test "tokenize sequence":
    var t = stringTokenizer "1 2 3 4 5"
    let actual = readSeq t
    check actual.isSome
    check get(actual) == @[1.0, 2.0, 3.0, 4.0, 5.0]

  test "all tokens":
    var t = stringTokenizer "key : value \n matrix 1 2 3"

    check t.word.isSome
    check get(t.word) == "key"
    nextToken t

    check t.word.isSome
    check get(t.word) == "value"
    nextToken t

    check t.word.isSome
    check get(t.word) == "matrix"
    nextToken t

    check t.val.isSome
    let s = readSeq t
    check s.isSome
    check get(s) == @[1.0, 2.0, 3.0]
    nextToken t

proc checkSpecDefaults(s: Spec) {.inline.} =
    check s.name == ""
    check s.comment == ""
    check s.kind == Tsp
    check s.dimention == 0
    check s.capacity == 0
    check s.edgeKind == Explicit
    check s.edgeFormat == FullMatrix
    check s.edgeData == EdgeList
    check s.nodeCoord == NoCoord
    check s.displayData == NoDisplay

suite "spec":
  test "default values":
    ## Not sure if this is even a good idea, but I can't think of an alternative
    checkSpecDefaults(Spec())

  test "set string":
    var s = Spec()
    check s.trySet("NAME", "steve")
    check s.name == "steve"

  test "set int":
    var s = Spec()
    check s.trySet("DIMENSION", "5")
    check s.dimention == 5

  test "set enum":
    var s = Spec()
    check s.trySet("DISPLAY_DATA_TYPE", "TWOD_DISPLAY")
    check s.displayData == DisplayType.Two

  test "set garbage":
    var s = Spec()
    check not s.trySet("whatever", "trash")
    checkSpecDefaults s

  test "parse empty":
    var t = stringTokenizer ""
    var s = parseSpec t
    checkSpecDefaults(get s)

  test "idempotent of consumed input":
    var t = stringTokenizer ""
    var s = parseSpec t
    checkSpecDefaults(get s)
    s = parseSpec t
    checkSpecDefaults(get s)

  test "parse key value (string)":
    var t = stringTokenizer "NAME : att48"
    let s = parseSpec t
    check s.isSome
    check get(s).name == "att48"

  test "parse key value (int)":
    var t = stringTokenizer "DIMENSION : 48"
    let s = parseSpec t
    check s.isSome
    check get(s).dimention == 48

  test "parse key value (enum)":
    var t = stringTokenizer "EDGE_WEIGHT_TYPE : ATT"
    let s = parseSpec t
    check s.isSome
    check get(s).edgeKind == Att

  test "parse sample spec":
    var t = stringTokenizer """
    NAME : att48
    COMMENT : 48 capitals of the US (Padberg/Rinaldi)
    TYPE : TSP
    DIMENSION : 48
    EDGE_WEIGHT_TYPE : ATT""".unindent()
    let s = parseSpec t
    check s.isSome
    let spec = get s
    check spec.name == "att48"
    check spec.comment == "48 capitals of the US (Padberg/Rinaldi)"
    check spec.kind == Tsp
    check spec.dimention == 48
    check spec.edgeKind == Att

suite "data":
  test "try set":
    var d = Data()
    check d.trySet("NODE_COORD_SECTION", @[1.0, 3, 1, 3])
    check d.nodeCoord == @[1.0, 3, 1, 3]

    check d.trySet("EDGE_WEIGHT_SECTION", @[1.0, 2, 3])
    check d.edgeWeight == @[1.0, 2, 3]

    check not d.trySet("EOF", @[])
    check not d.trySet("garbade", @[])

  test "parse line":
    var t = stringTokenizer """
    NODE_COORD_SECTION
    1 6734 1453""".unindent()

    let d = parseData t
    check d.isSome

    let data = get d
    check data.nodeCoord == @[1.0, 6734, 1453]

  test "parse section":
    var t = stringTokenizer """
    NODE_COORD_SECTION
    1 6734 1453
    2 2233 10
    3 5530 1424
    4 401 841
    5 3082 1644""".unindent()

    let d = parseData t
    check d.isSome

    let data = get d
    check data.nodeCoord == @[
      1.0, 6734, 1453,
      2, 2233, 10,
      3, 5530, 1424,
      4, 401, 841,
      5, 3082, 1644]

suite "intergration spec + data":
  test "parse spec then data":
    var t = stringTokenizer """
    NAME: ulysses16.tsp
    TYPE: TSP
    DIMENSION: 16
    NODE_COORD_SECTION
    1 38.24 20.42
    2 39.57 26.15""".unindent()

    let s = parseSpec t
    check s.isSome
    let spec = get s
    check spec.name == "ulysses16.tsp"
    check spec.kind == Tsp
    check spec.dimention == 16

    let d = parseData t
    check d.isSome
    let data = get d
    check data.nodeCoord == @[
      1.0, 38.24, 20.42,
      2.0, 39.57, 26.15]

suite "instances":
  test "empty graph":
    check newGraphProblem(Spec(), Data()).isSome

  test "empty tour":
    check newTourInstance(Spec(), Data()).isSome

suite "file instances":
  test "just parse":
    let sd = parseFile("data/ulysses16.tsp")
    check sd.isSome
    let specData = get sd
    let spec = specData[0]
    check spec.name == "ulysses16.tsp"
    check spec.kind == Tsp
    check spec.dimention == 16

    let data = specData[1]
    check data.nodeCoord == @[
      1.0, 38.24, 20.42,
      2, 39.57, 26.15,
      3, 40.56, 25.32,
      4, 36.26, 23.12,
      5, 33.48, 10.54,
      6, 37.56, 12.19,
      7, 38.42, 13.11,
      8, 37.52, 20.44,
      9, 41.23, 9.10,
      10, 41.17, 13.05,
      11, 36.08, -5.21,
      12, 38.47, 15.13,
      13, 38.15, 15.35,
      14, 37.51, 15.17,
      15, 35.49, 14.32,
      16, 39.36, 19.56]

  test "parse tsp instance":
    let g = parseProblem "data/ulysses16.tsp"
    check g.isSome

  test "parse tour":
    let g = parseTour "data/att48.opt.tour"
    check g.isSome
